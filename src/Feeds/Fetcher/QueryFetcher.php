<?php

namespace Drupal\feeds_ldap\Feeds\Fetcher;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\StateInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\ldap_query\Controller\QueryController;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds_ldap\Feeds\Result\QueryFetcherResult;

/**
 * Defines an SQL fetcher.
 *
 * @FeedsFetcher(
 *   id = "feeds_ldap_query_fetcher",
 *   title = @Translation("LDAP Query Fetcher"),
 *   description = @Translation("Fetch content from ldap query"),
 *   form = {
 *     "configuration" = "Drupal\feeds_ldap\Feeds\Fetcher\Form\QueryFetcherForm",
 *     "feed" = "Drupal\feeds_ldap\Feeds\Fetcher\Form\QueryFetcherFeedForm",
 *   },
 *   arguments = {"@cache.feeds_download", "@messenger"}
 * )
 */
class QueryFetcher extends PluginBase implements FetcherInterface {

  use StringTranslationTrait;

  /**
   * Results to import per run.
   */
  const PER_RUN = 100;

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $results = [];

    if (empty($state->raw_results)) {
      $query_controller = \Drupal::service('ldap.query');
      $query_controller->load($feed->config->fetcher['query']);
      $query_controller->execute();
      $state->raw_results = $query_controller->getRawResults();

      if (empty($state->raw_results)) {
        \Drupal::logger('feeds_ldap')->error(
          t('LDAP feeds aborted import of feed @feed_id due to receiving no LDAP results. This was almost definitely not intended and could have resulted in mass blocking of users.')
        );
        throw new \RuntimeException('No LDAP results returned! Aborting feed import to prevent mass blocking.');
      }

      $last_raw_result_state_name = 'feeds_ldap_last_raw_result_count_' . $feed->id();
      $last_raw_result_count = \Drupal::state()
        ->get($last_raw_result_state_name, FALSE);
      if ($last_raw_result_count) {
        $minimum_result_count = $last_raw_result_count * 0.75;
        if (count($state->raw_results) < $minimum_result_count) {
          \Drupal::logger('feeds_ldap')->error(
            t(
              'LDAP feeds aborted import of feed @feed_id due to receiving far fewer LDAP results than expected. If there was legitimately a large decrease in user count you can forget this count by deleting the @state_name state, e.g. with `drush sdel @state_name` and reset the feed with `drush feeds:unlock @feed_id`.',
              [
                '@state_name' => $last_raw_result_state_name,
                '@feed_id' => $feed->id(),
              ]
            )
          );
          throw new \RuntimeException('Far fewer LDAP results were returned than expected! Aborting feed import to prevent mass blocking.');
        }
        else {
          \Drupal::state()->set($last_raw_result_state_name, count($state->raw_results));
        }
      }

      $state->total = count($state->raw_results);
      $state->progress = 0;
    }

    $processed = 0;
    $raw_results_slice = array_splice($state->raw_results, 0, self::PER_RUN);
    foreach ($raw_results_slice as $raw_result) {
      $results[] = $raw_result;
      ++$processed;
    }

    $state->progress($state->total, $processed);

    if (empty($state->raw_results)) {
      $state->setCompleted();
    }

    return new QueryFetcherResult($results);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'queries' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration() {
    return [
      'query' => '',
    ];
  }

}
